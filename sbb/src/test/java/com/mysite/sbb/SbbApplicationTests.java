package com.mysite.sbb;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SbbApplicationTests {

	@Autowired
	private QuestionRepository questionRepository;
	
	@Test
	void contextLoads() {
		Question q1 = new Question();
		q1.setSubject("SpringBoot가 무엇인가요?");
		q1.setContent("SpringBoot에 대해서 알고싶어요.");
		q1.setCreateDate(LocalDateTime.now());
		this.questionRepository.save(q1); // 질문1 저장

		Question q2 = new Question();
		q2.setSubject("JPA가 무엇인가요?");
		q2.setContent("JPA에 대해서 알고싶어요.");
		q2.setCreateDate(LocalDateTime.now());
		this.questionRepository.save(q2); // 질문2 저장

	}

}
