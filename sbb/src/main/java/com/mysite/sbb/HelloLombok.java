package com.mysite.sbb;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/*
 * 롬복의 어노테이션 RequiredArgsConstructor
 * final 필드에 대한 생성자 자동 생성
 */

@RequiredArgsConstructor
@Getter
public class HelloLombok {
	private final String hello;
	private final int lombok;
	
	public static void main(String[] args) {
		HelloLombok helloLombok = new HelloLombok("헬로", 5);
		System.out.println(helloLombok.getHello());
		System.out.println(helloLombok.getLombok());
	}
}
