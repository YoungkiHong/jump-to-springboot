package com.mysite.sbb;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Answer {
	/*
	 * @ManyToOne : 다른 엔티티를 참조하기 위한 애너테이션, N:1 관계
	 * 질문은 1개, 답변은 N개
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(columnDefinition = "TEXT")
	private String content;
	
	private LocalDateTime createDate;
	
	// 질문 엔티티를 참조하는 속성
	// 타입인 Question을 보고 JPA가 Question을 참조한다
	@ManyToOne
	private Question question;
	
}
