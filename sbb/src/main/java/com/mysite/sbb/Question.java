package com.mysite.sbb;

import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Question {
	/*
	 * @Id : id를 기본키로 지정하는 애너테이션
	 * @GeneratedValue : 자동으로 1씩 더해서 저장하는 애너테이션
	 * strategy = GenerationType.IDENTITY : 해당 속성만 번호가 증가하도록 지정
	 * @Column : 엔티티 속성(테이블 열)의 세부 설정을 위한 애너테이션
	 * length : 열의 길이 설정
	 * columnDefinition : 열의 유형 설정
	 * @OneToMany : 다른 엔티티를 참조하기 위한 애너테이션, 1:N 관계
	 * mappedBy : 참조할 엔티티의 속성 지정
	 * cascade = CascadeType.REMOVE : 질문이 삭제되면 답변도 모두 삭제되도록 설정
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(length = 200)
	private String subject;
	
	@Column(columnDefinition = "TEXT")
	private String content;
	
	// DB는 카멜케이스 적용X, createDate -> create_date 이와같이 변경됨
	private LocalDateTime createDate;
	
	// Answer 엔티티의 question이 mappedBy에 전달됨
	@OneToMany(mappedBy = "question", cascade = CascadeType.REMOVE)
	private List<Answer> answerList;
	
}
